local lint = require('lint')

lint.linters_by_ft = {
  typescript = { "eslint_d", },
  javascript = { "eslint_d", },
  svelte = { "eslint_d", },
  javascriptreact = { "eslint_d", },
  typescriptreact = { "eslint_d", },
  astro = { "eslint_d", },
}

vim.api.nvim_create_autocmd({ "TextChanged" }, {
  callback = function()
    require("lint").try_lint()
  end,
})

require('formatter').setup({
  logging = true,
  log_level = vim.log.levels.DEBUG,
  filetype = {
    typescript = require('formatter.filetypes.typescript').prettierd,
    typescriptreact = require('formatter.filetypes.typescriptreact').prettierd,
    javascript = require('formatter.filetypes.javascript').prettierd,
    javascriptreact = require('formatter.filetypes.javascriptreact').prettierd,
    svelte = {
      function ()
        return {
          exe = './node_modules/.bin/prettier',
          args = { '--stdin-filepath', '"' .. vim.api.nvim_buf_get_name(0) .. '"' },
          stdin = true
        }
      end
    },
    astro = {
      function ()
        return {
          exe = './node_modules/.bin/prettier',
          args = { '--stdin-filepath', vim.api.nvim_buf_get_name(0) },
          stdin = true
        }
      end
    }
  }
})

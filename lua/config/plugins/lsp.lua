local caps = require("cmp_nvim_lsp").default_capabilities()

require("typescript").setup({
  capabilities = caps,
})

require("lspconfig").astro.setup({
  capabilities = caps,
})

require("lspconfig").lua_ls.setup({
  capabilities = caps,
  settings = {
    Lua = {
      runtime = {
        version = "LuaJIT",
      },
      diagnostics = {
        globals = { "vim" },
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file("", true),
      },
      telemetry = {
        enable = false,
      },
    },
  },
})

require("lspconfig").pyright.setup({
  capabilities = caps,
})

require("lspconfig").rust_analyzer.setup({
  capabilities = caps,
})

require("lspconfig").tailwindcss.setup({
  capabilities = caps,
  settings = {
    tailwindCSS = {
      experimental = {
        classRegex = {
          { "cva\\(([^)]*)\\)", "[\"'`]([^\"'`]*).*?[\"'`]" },
          { "cx\\(([^)]*)\\)", "(?:'|\"|`)([^']*)(?:'|\"|`)" }
        }
      }
    }
  }
})

require("lspconfig").bashls.setup({
  capabilities = caps,
})

require("lspconfig").prismals.setup({
  capabilities = caps,
})

require("lspconfig").svelte.setup({
  capabilities = caps,
})

require("lspconfig").html.setup({
  capabilities = caps,
})

require("lspconfig").ocamllsp.setup({
  capabilities = caps,
})

require("lspconfig").jsonls.setup({
  capabilities = caps,
  settings = {
    json = {
      schemas = {
        {
          fileMatch = { 'package.json' },
          url = 'https://json.schemastore.org/package.json',
        },
      },
    },
  },
})

require("lspconfig").yamlls.setup({
  capabilities = caps,
})

-- TreeSitter Configs

require("nvim-treesitter.configs").setup({
  highlight = {
    enable = true,
  },
  indent = {
    enable = true,
    disable = { "python" },
  },
  autotag = {
    enable = true,
  },
})

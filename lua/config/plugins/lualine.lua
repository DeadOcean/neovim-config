require("lualine").setup({
	options = {
		theme = "kanagawa",
		section_separators = { left = " ", right = " " },
		component_separators = { left = "", right = "" },
	},
	extensions = {
		"neo-tree",
	},
})

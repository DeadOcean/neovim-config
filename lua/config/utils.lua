local M = {}

---@param prefix string
M.unload_lua_namespace = function(prefix)
  local prefix_with_dot = prefix .. "."
  for key, _ in pairs(package.loaded) do
    if key == prefix or key:sub(1, #prefix_with_dot) == prefix_with_dot then
      package.loaded[key] = nil
    end
  end
end

M.has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

---@param amount number
---@param guifont string
M.change_font_size = function(guifont, amount)
  local separator_index = string.find(guifont, ":h")
  local font_name = string.gsub(string.sub(guifont, 1, separator_index - 1), " ", "\\ ")
  local font_size = tonumber(string.sub(guifont, separator_index + 2))
  font_size = font_size + amount
  vim.cmd("set guifont=" .. font_name .. ":h" .. font_size)
end

return M
